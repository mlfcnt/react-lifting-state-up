import React, { Component } from "react";
import { Map, CircleMarker, TileLayer } from "react-leaflet";
import "leaflet/dist/leaflet.css";

class LeafMap extends Component {
  state = {};
  render() {
    return (
      <div>
        <Map
          style={{ height: "480px", width: "100%" }}
          zoom={0.5}
          center={[-0.09, 51.505]}
          className="m-4"
        >
          <TileLayer url="http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />

          <CircleMarker center={this.props.coordinates} />
        </Map>
      </div>
    );
  }
}

export default LeafMap;
