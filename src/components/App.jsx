import React, { Component } from "react";
import Card from "./Card";
import Jumbotron from "./Jumbotron";
import LeafMap from "./LeafMap";
import { Map, CircleMarker, TileLayer } from "react-leaflet";
import "leaflet/dist/leaflet.css";
import PlusDinfos from "./PlusDinfos";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      loading: true,
      selected: false,
      display: true
    };
  }
  async componentDidMount() {
    fetch("https://randomuser.me/api/?results=10")
      .then(res => res.json())
      .then(json => {
        this.setState({
          items: json,
          loading: false
        });
      });
  }
  render() {
    var { items, loading } = this.state;

    if (loading) {
      return (
        <div>
          <Jumbotron />
          <div className="mx-auto col-12 text-center">
            <img src="reload.svg" alt="loading animation" />
          </div>
        </div>
      );
    } else {
      // var latitude = items.results[0].location.coordinates.latitude;
      // var longitude = items.results[0].location.coordinates.longitude;
      // var coordinates = [latitude, longitude];

      return (
        <div className="App">
          <Jumbotron />
          <div className="container">
            {/*! BLOC CRIMINEL */}
            <div className="criminal my-4">
              <div className="col-sm-12 my-4">
                <Card
                  items={items.results[0]}
                  selected={this.state.selected}
                  display={this.state.display}
                />
              </div>
            </div>
            {/*! FIN BLOC CRIMINAL  */}
            {/*! BLOC CRIMINEL */}
            <div className="criminal my-4">
              <div className="col-sm-12 my-4">
                <Card
                  items={items.results[1]}
                  selected={this.state.selected}
                  display={this.state.display}
                />
              </div>
            </div>
            {/*! FIN BLOC CRIMINAL  */}
            {/*! BLOC CRIMINEL */}
            <div className="criminal my-4">
              <div className="col-sm-12 my-4">
                <Card
                  items={items.results[2]}
                  selected={this.state.selected}
                  display={this.state.display}
                />
              </div>
            </div>
            {/*! FIN BLOC CRIMINAL  */}
            {/*! BLOC CRIMINEL */}
            <div className="criminal my-4">
              <div className="col-sm-12 my-4">
                <Card
                  items={items.results[3]}
                  selected={this.state.selected}
                  display={this.state.display}
                />
              </div>
            </div>
            {/*! FIN BLOC CRIMINAL  */}
            {/*! BLOC CRIMINEL */}
            <div className="criminal my-4">
              <div className="col-sm-12 my-4">
                <Card
                  items={items.results[4]}
                  selected={this.state.selected}
                  display={this.state.display}
                />
              </div>
            </div>
            {/*! FIN BLOC CRIMINAL  */}
            {/*! BLOC CRIMINEL */}
            <div className="criminal my-4">
              <div className="col-sm-12 my-4">
                <Card
                  items={items.results[5]}
                  selected={this.state.selected}
                  display={this.state.display}
                />
              </div>
            </div>
            {/*! FIN BLOC CRIMINAL  */}
            {/*! BLOC CRIMINEL */}
            <div className="criminal my-4">
              <div className="col-sm-12 my-4">
                <Card
                  items={items.results[6]}
                  selected={this.state.selected}
                  display={this.state.display}
                />
              </div>
            </div>
            {/*! FIN BLOC CRIMINAL  */}
            {/*! BLOC CRIMINEL */}
            <div className="criminal my-4">
              <div className="col-sm-12 my-4">
                <Card
                  items={items.results[7]}
                  selected={this.state.selected}
                  display={this.state.display}
                />
              </div>
            </div>
            {/*! FIN BLOC CRIMINAL  */}
            {/*! BLOC CRIMINEL */}
            <div className="criminal my-4">
              <div className="col-sm-12 my-4">
                <Card
                  items={items.results[8]}
                  selected={this.state.selected}
                  display={this.state.display}
                />
              </div>
            </div>
            {/*! FIN BLOC CRIMINAL  */}
            {/*! BLOC CRIMINEL */}
            <div className="criminal my-4">
              <div className="col-sm-12 my-4">
                <Card
                  items={items.results[9]}
                  selected={this.state.selected}
                  display={this.state.display}
                />
              </div>
            </div>
            {/*! FIN BLOC CRIMINAL  */}
          </div>
        </div>
      );
    }
  }
}

export default App;
