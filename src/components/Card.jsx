import React, { Component } from "react";
import LeafMap from "./LeafMap";
import PlusDinfos from "./PlusDinfos";

class Card extends Component {
  state = {
    id: null,
    coordinates: null,
    displayMore: false,
    displayedAtAll: true
  };
  style = {
    width: "18rem"
  };
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      selected: false,
      display: true
    };
  }

  render() {
    function jsUcfirst(string) {
      return string.charAt(0).toUpperCase() + string.slice(1);
    }

    const handleToggleMap = () => {
      this.state.id = this.props.items.login.md5;
      this.state.latitude = this.props.items.location.coordinates.latitude;
      this.state.longitude = this.props.items.location.coordinates.longitude;
      this.state.coordinates = [this.state.latitude, this.state.longitude];
      this.setState(() => {
        return {
          selected: !this.state.selected,
          coordinates: this.state.coordinates
        };
      });
    };

    const handlePlusDinfos = () => {
      this.setState(() => {
        return {
          displayMore: !this.state.displayMore
        };
      });
    };

    const handleClose = () => {
      this.setState(() => {
        return {
          displayAtAll: false
        };
      });
    };

    if (!this.state.selected && !this.state.displayMore) {
      return (
        <div className="card" style={this.style}>
          <img
            className="card-img-top"
            src={this.props.items.picture.large}
            alt="Card image cap"
          />
          <div className="card-body">
            <h5 className="card-title">
              <span> {jsUcfirst(this.props.items.name.title)}</span>
              <span> {jsUcfirst(this.props.items.name.first)}</span>
              <span> {jsUcfirst(this.props.items.name.last)}</span>
            </h5>
            <p className="card-text"> Age : {this.props.items.dob.age}</p>
            <button
              type="button"
              className="btn btn-secondary m-2"
              onClick={handlePlusDinfos}
            >
              Plus d'infos
            </button>
            <button
              type="button"
              className="btn btn-primary m-2"
              onClick={handleToggleMap}
            >
              Dernière position connue
            </button>
            <button
              type="button"
              className="btn btn-danger m-2"
              onClick={handleClose}
            >
              Supprimer cette entrée
            </button>
          </div>
        </div>
      );
    } else if (this.state.selected && !this.state.displayMore) {
      return (
        <div>
          <div className="card" style={this.style}>
            <img
              className="card-img-top"
              src={this.props.items.picture.large}
              alt="Card image cap"
            />
            <div className="card-body">
              <h5 className="card-title">
                <span> {jsUcfirst(this.props.items.name.title)}</span>
                <span> {jsUcfirst(this.props.items.name.first)}</span>
                <span> {jsUcfirst(this.props.items.name.last)}</span>
              </h5>
              <p className="card-text"> Age : {this.props.items.dob.age}</p>
              <button
                type="button"
                className="btn btn-secondary m-2"
                onClick={handlePlusDinfos}
              >
                Plus d'infos
              </button>
              <button
                type="button"
                className="btn btn-warning m-2"
                onClick={handleToggleMap}
              >
                Fermer la carte
              </button>
              <button
                type="button"
                className="btn btn-danger m-2"
                onClick={handleClose}
              >
                Supprimer cette entrée
              </button>
            </div>
          </div>
          <LeafMap coordinates={this.state.coordinates} />
        </div>
      );
    } else if (!this.state.selected && this.state.displayMore) {
      return (
        <div className="row">
          <div className="card col-6" style={this.style}>
            <img
              className="card-img-top"
              src={this.props.items.picture.large}
              alt="Card image cap"
            />
            <div className="card-body">
              <h5 className="card-title">
                <span> {jsUcfirst(this.props.items.name.title)}</span>
                <span> {jsUcfirst(this.props.items.name.first)}</span>
                <span> {jsUcfirst(this.props.items.name.last)}</span>
              </h5>
              <p className="card-text"> Age : {this.props.items.dob.age}</p>
              <button
                type="button"
                className="btn btn-secondary m-2"
                onClick={handlePlusDinfos}
              >
                Moins d'infos
              </button>
              <button
                type="button"
                className="btn btn-primary m-2"
                onClick={handleToggleMap}
              >
                Dernière position connue
              </button>
              <button
                type="button"
                className="btn btn-danger m-2"
                onClick={handleClose}
              >
                Supprimer cette entrée
              </button>
            </div>
          </div>
          <div className="col-6">
            <PlusDinfos items={this.props.items} />
          </div>
        </div>
      );
    } else if (this.state.selected && this.state.displayMore) {
      return (
        <div>
          <div className="row">
            <div className="card col-6" style={this.style}>
              <img
                className="card-img-top"
                src={this.props.items.picture.large}
                alt="Card image cap"
              />
              <div className="card-body">
                <h5 className="card-title">
                  <span> {jsUcfirst(this.props.items.name.title)}</span>
                  <span> {jsUcfirst(this.props.items.name.first)}</span>
                  <span> {jsUcfirst(this.props.items.name.last)}</span>
                </h5>
                <p className="card-text"> Age : {this.props.items.dob.age}</p>
                <button
                  type="button"
                  className="btn btn-secondary m-2"
                  onClick={handlePlusDinfos}
                >
                  Moins d'infos
                </button>
                <button
                  type="button"
                  className="btn btn-warning m-2"
                  onClick={handleToggleMap}
                >
                  Fermer la carte
                </button>
                <button
                  type="button"
                  className="btn btn-danger m-2"
                  onClick={handleClose}
                >
                  Supprimer cette entrée
                </button>
              </div>
            </div>
            <div className="col-6">
              <PlusDinfos items={this.props.items} />
            </div>
          </div>
          <LeafMap coordinates={this.state.coordinates} />
        </div>
      );
    }
  }
}

export default Card;
