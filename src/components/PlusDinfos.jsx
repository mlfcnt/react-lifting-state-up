import React, { Component } from "react";

class PlusDinfos extends Component {
  state = {};
  render() {
    return (
      <div className="border p-4">
        <p>Nationalité : {this.props.items.nat}</p>
        <p>Date de naissance: {this.props.items.dob.date}</p>
        <p>
          Dernière adresse connue : {this.props.items.location.street}{" "}
          {this.props.items.location.postcode} {this.props.items.location.city}{" "}
          {this.props.items.location.state}
        </p>
        <p>Email : {this.props.items.email}</p>
        <p>Aussi connue sous le nom de : {this.props.items.login.username}</p>
      </div>
    );
  }
}

export default PlusDinfos;
