import React, { Component } from "react";

class Jumbotron extends Component {
  state = {};
  render() {
    return (
      <div className="jumbotron jumbotron-fluid">
        <div className="container">
          <h1 className="display-4">FBI.com</h1>
          <p className="lead">
            ATTENTION ! Ces personnes sont activement recherchées et sont
            potientiellement dangereuses.
          </p>
          <p>
            Si vous avez quelconque information qui pourrait nous aider à les
            retrouver, contactez FBI@simplon.com
          </p>
        </div>
      </div>
    );
  }
}

export default Jumbotron;
