import React from "react";
import "./App.css";
import App from "./components/App";

function App() {
  return (
    <div className="App">
      <App />
    </div>
  );
}

export default App;
